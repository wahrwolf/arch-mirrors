import tomllib
import tomli_w
from collections import defaultdict

from ...tooling_help import (
	new_defaultdict,
	defaultdict_to_dict,
	tomlify
)

from ...models.versions import VersionParsers


def verify_upstreams(mirrors, _mirrors_copy=None):
	"""
	Cycles through all mirrors and verifies that their
	individual upstreams exists.

	:param dict mirrors:
	  A complete set of mirrors loaded from all TOML files.
	:param dict _mirrors_copy: Optional;
	  A copy of :code:`mirrors`, set internally for recursive purposes only.
	"""
	if not _mirrors_copy:
		_mirrors_copy = mirrors

	for key, val in mirrors.items():
		if isinstance(val, dict):
			verify_upstreams(val, _mirrors_copy)
		
		for version in VersionParsers.list():
			if isinstance(val, version):
				if hasattr(val, 'upstream') and val.upstream:
					paths = val.upstream.split('.')
					current_path_in_mirrors = _mirrors_copy
					for path in paths:
						if current_path_in_mirrors.get(path) is None:
							raise ValueError(f"Mirror '{val.name}'s upstream '{val.upstream}' ('{path}') does not exist")
						else:
							current_path_in_mirrors = current_path_in_mirrors[path]


def verify(args):
	"""
	Called via :code:`arch-mirrors verify`.
	Verifies all new and existing TOML files for their integrity.
	This is done by verifying different dynamic values that can only
	be checked after the complete set of mirrors are loaded.

	One such example is the upstream reference, it can only be verified
	after all mirrors are loaded (without these verification steps).
	"""
	mirrors = defaultdict(new_defaultdict)

	for path in args.directory.rglob('*.toml'):
		# We skip the first level to avoid master.toml
		# and pyproject.toml to be read
		if path.parent == args.directory:
			continue

		with path.open('rb') as fh:
			data = tomllib.load(fh)

		country = path.parent
		region = country.parent

		if f"{region}" == ".":
			region = country.name
			country = None
		else:
			region = region.name
			country = country.name

		mirror_object = VersionParsers[args.version].value(**data)

		if country:
			mirrors[region][country][mirror_object.name] = mirror_object
		else:
			mirrors[region][mirror_object.name] = mirror_object

	verify_upstreams(mirrors)